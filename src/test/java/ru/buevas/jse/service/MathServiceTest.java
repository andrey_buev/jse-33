package ru.buevas.jse.service;

import java.math.BigInteger;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class MathServiceTest {

    private MathService mathService = new MathService();
    private String threadsCount = "4";

    @Test
    void sumSuccess() {
        assertEquals(20L, mathService.sum("10", "10"));
    }

    @Test
    void sumArgNotNumber() {
        assertThrows(IllegalArgumentException.class, () -> mathService.sum("a", "b"));
    }

    @Test
    void factorialSuccess() {
        assertEquals(BigInteger.valueOf(479001600), mathService.factorial("12", threadsCount));
    }

    @Test
    void factorialNonPositiveArg() {
        assertThrows(IllegalArgumentException.class, () -> mathService.factorial("-10", threadsCount));
    }

    @Test
    void factorialArgNotNumber() {
        assertThrows(IllegalArgumentException.class, () -> mathService.factorial("n", threadsCount));
    }

    @Test
    void fibonacciSuccess() {
        long[] expected = new long[] {0, 1, 1, 2, 3, 5, 8, 13, 21};
        assertArrayEquals(expected, mathService.fibonacci("54"));
    }

    @Test
    void fibonacciNoSequence() {
        assertThrows(IllegalArgumentException.class, () -> mathService.fibonacci("55"));
    }

    @Test
    void fibonacciNonPositiveArg() {
        assertThrows(IllegalArgumentException.class, () -> mathService.fibonacci("-10"));
    }

    @Test
    void fibonacciArgNotNumber() {
        assertThrows(IllegalArgumentException.class, () -> mathService.fibonacci("n"));
    }
}