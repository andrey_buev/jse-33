package ru.buevas.jse;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.logging.Level;
import lombok.extern.java.Log;
import ru.buevas.jse.service.MathService;

@Log
public class Main {

    public static void main(String[] args) {
        MathService mathService = new MathService();

        boolean isExit = false;
        try (BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in))) {
            while (!isExit) {
                log.log(Level.INFO, "Commands:");
                log.log(Level.INFO, "1) sum");
                log.log(Level.INFO, "2) factorial");
                log.log(Level.INFO, "3) fibonacci");
                log.log(Level.INFO, "4) exit");
                log.log(Level.INFO, "enter command number:");
                String commandNumber = bufferedReader.readLine();
                String arg1;
                String arg2;
                switch (commandNumber) {
                    case "1":
                        log.log(Level.INFO, "Sum");
                        log.log(Level.INFO, "Enter arg1:");
                        arg1 = bufferedReader.readLine();
                        log.log(Level.INFO, "Enter arg2:");
                        arg2 = bufferedReader.readLine();
                        log.log(Level.INFO,
                                String.format("Sum %s and %s = %s", arg1, arg2, mathService.sum(arg1, arg2)));
                        break;
                    case "2":
                        log.log(Level.INFO, "Factorial");
                        log.log(Level.INFO, "Enter arg:");
                        arg1 = bufferedReader.readLine();
                        log.log(Level.INFO, "Enter thread count:");
                        arg2 = bufferedReader.readLine();
                        log.log(Level.INFO,
                                String.format("Factorial of %s = %s", arg1, mathService.factorial(arg1, arg2)));
                        break;
                    case "3":
                        log.log(Level.INFO, "Fibonacci");
                        log.log(Level.INFO, "Enter arg:");
                        arg1 = bufferedReader.readLine();
                        log.log(Level.INFO,
                                String.format("FibonacciSequence for %s = %s",
                                        arg1,
                                        Arrays.toString(mathService.fibonacci(arg1))));
                        break;
                    case "4":
                        log.log(Level.INFO, "Exit");
                        isExit = true;
                        break;
                    default:
                        log.log(Level.SEVERE, "No such command");
                }
            }
        } catch (IOException | IllegalArgumentException e) {
            log.log(Level.SEVERE, e.getMessage());
        }
    }
}
