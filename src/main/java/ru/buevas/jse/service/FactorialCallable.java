package ru.buevas.jse.service;

import java.math.BigInteger;
import java.util.concurrent.Callable;

public class FactorialCallable implements Callable<BigInteger> {
    private int from;
    private int to;

    public FactorialCallable(int from, int to) {
        this.from = from;
        this.to = to;
    }

    @Override
    public BigInteger call() {
        BigInteger taskResult = BigInteger.ONE;
        for (int j = from; j <= to; j++) {
            taskResult = taskResult.multiply(j == 0 ? BigInteger.ONE : BigInteger.valueOf(j));
        }
        return taskResult;
    }
}
