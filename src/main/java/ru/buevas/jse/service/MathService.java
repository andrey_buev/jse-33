package ru.buevas.jse.service;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

public class MathService {
    private static final Logger log = Logger.getLogger(MathService.class.getName());

    public long sum(String arg1, String arg2) {
        return parseToLong(arg1) + parseToLong(arg2);
    }

    public BigInteger factorial(String arg, String threadsCount) {
        Integer parsedArg = parseToInt(arg);
        Integer parsedThreadCount = parseToInt(threadsCount);
        isPositive(parsedArg);

        BigInteger result = BigInteger.ONE;
        ExecutorService executorService = Executors.newFixedThreadPool(parsedThreadCount);
        List<Callable<BigInteger>> tasks = new ArrayList<>();

        int partSize = parsedArg / parsedThreadCount;

        for (int i = 0; i < parsedThreadCount; i++) {
            tasks.add(new FactorialCallable(partSize * i, partSize * (i + 1) - 1));
        }
        tasks.add(new FactorialCallable(partSize * parsedThreadCount, parsedArg));
        try {
            List<Future<BigInteger>> futuresList = executorService.invokeAll(tasks);
            for (Future<BigInteger> future : futuresList) {
                result = result.multiply(future.get());
            }
        } catch (InterruptedException | ExecutionException e) {
            log.log(Level.SEVERE, e.getMessage());
            Thread.currentThread().interrupt();
        }
        return result;
    }

    public long[] fibonacci(String arg) {
        Long parsedArg = parseToLong(arg);
        isPositive(parsedArg);

        List<Long> sequence = new ArrayList<>();
        sequence.add(0L);
        long sum = 0;
        int n = 1;

        while (sum < parsedArg) {
            sequence.add(fibonacciNumber(++n));
            sum = sequence.stream().collect(Collectors.summingLong(Long::longValue));
        }

        if (sum == parsedArg) {
            return sequence.stream().mapToLong(Long::longValue).toArray();
        } else {
            throw new IllegalArgumentException("there is no sequence for the argument " + arg);
        }
    }

    private long fibonacciNumber(int n) {
        if (n == 1) {
            return 0L;
        }
        if (n == 2) {
            return 1L;
        }
        return fibonacciNumber(n - 1) + fibonacciNumber(n - 2);
    }

    private int parseToInt(String number) {
        try {
            return Integer.parseInt(number);
        } catch (NumberFormatException e) {
            throw new IllegalArgumentException(e.getMessage());
        }
    }

    private long parseToLong(String number) {
        try {
            return Long.parseLong(number);
        } catch (NumberFormatException e) {
            throw new IllegalArgumentException(e.getMessage());
        }
    }

    private void isPositive(int number) {
        if (number <= 0) {
            throw new IllegalArgumentException(String.format("Number %d < 0", number));
        }
    }

    private void isPositive(long number) {
        if (number <= 0) {
            throw new IllegalArgumentException(String.format("Number %f < 0", number));
        }
    }
}
